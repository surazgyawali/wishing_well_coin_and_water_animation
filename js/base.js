function getRandomNumber(min, max) {
    return Math.random() * (max - min) + min
}

function generate_sinked_coins() {
    var coin = document.getElementById('original_coin')

    var well = document.getElementById('well')
    // get window width and height
    var winHeight = window.innerHeight
    var winWidth = window.innerWidth
    coin.style.top = winHeight / 2 + "vh"
    coin.style.left = winWidth / 2 + "vw"
    coin.style.display = "block"
    coin.removeAttribute('id')

    var widthOffset = winWidth / 4
    var heightOffset = winHeight / 4

    for (var i = 0; i < 50; i++) {

        var clone = coin.cloneNode(true);
        // get random numbers for each element
        randomTop = getRandomNumber(0, 100);
        randomLeft = getRandomNumber(0, 100);

        // update top and left position
        clone.style.top = randomTop + "vh";
        clone.style.left = randomLeft + "vw";

        var coinSize = getRandomNumber(2, 2.5)
        clone.style.width = coinSize + "rem";
        clone.style.height = coinSize + "rem";

        var coinOpacity = getRandomNumber(.8, 1)
        clone.style.opacity = coinOpacity
        var items = ["silver", null]
        var item = items[Math.floor(Math.random() * items.length)];
        clone.classList.add(item)
        well.appendChild(clone)

    }
}

function make_a_wish() {
    // do stuffs
    $('#submit_wish').click(function () {
        var amount = $('#amount').val()
        var currency = $('#currency').val()
        var wish_title = $('#wish-title').val()
        var wish_text = $('#wish-text').val()
        var is_public = $('#is_public').is(':checked')
        $('#wishModal').modal('hide')
    })
}


function coin_drop(amount, currency, wish_title, wish_text, is_public) {
    var body = document.getElementsByTagName('body')[0]
    var well = document.getElementById('well')
    var coin = document.getElementById('drop_coin');
    var clone = coin.cloneNode(true);

    var randomTop = getRandomNumber(20, 80);
    var randomLeft = getRandomNumber(20, 80);

    // get window width and height
    clone.style.top = randomTop + "vh";
    clone.style.left = randomLeft + "vw"

    var coinSize = 9

    clone.style.width = coinSize + "rem";
    clone.style.height = coinSize + "rem";

    var items = ["silver", null]
    var item = items[Math.floor(Math.random() * items.length)];
    if (item) {
        clone.classList.add(item)
    }
    body.appendChild(clone)
    clone_in_water = clone.cloneNode(true);
    clone_in_water.style.display = "hidden"
    clone.removeAttribute('id')
    clone_in_water.removeAttribute('id')

    well.appendChild(clone_in_water)
    setTimeout(function () {
        clone.parentElement.removeChild(clone)
        clone_in_water.style.display = "block"

        var coinSize = getRandomNumber(2, 2.5)
        clone_in_water.style.width = coinSize + "rem"
        clone_in_water.style.height = coinSize + "rem"
        clone_in_water.classList.add('coin_static')
        var rect = clone_in_water.getBoundingClientRect()
        // below lines can be used to change the size and impact of the coin.
        // You can add drops programmatically by doing
        // $('body').ripples("drop", x, y, radius, strength)
        // where x and y are the coordinates of the drop, radius is the size of the drop, and strength is the strength of the drop.
        $('body').ripples('drop', rect.left, rect.top, 10, .5)
        clone_in_water.classList.remove('drop-coin')

        // to show wish onclick
        show_wish(clone_in_water, amount, currency, wish_title, wish_text, is_public)
        // to expire wish display
        expire_wish_display(clone_in_water, 10)
    }, 550)

}

function show_wish(coin_elm, amount, currency, wish_title, wish_text, is_public) {
    coin_elm.onclick = function () {
        if (is_public) {
            $('#wtitle-display').text(wish_title)
            $('#wamount-display').text(amount + " " + currency)
            $('#wtext-display').text(wish_text)
            $('#displayWishModal').modal('toggle')
        }
    }
}

function expire_wish_display(coin_elm,expireTimeinSeconds) {
    expireTimeinSeconds = expireTimeinSeconds * 1000
    setTimeout(function () {
        coin_elm.onclick = null
    }, expireTimeinSeconds)
}
